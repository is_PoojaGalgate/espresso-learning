package com.example.myapplication.test
import android.app.Activity
import android.content.Intent
import androidx.test.rule.ActivityTestRule
import com.example.myapplication.MainActivity
import cucumber.api.java.After
import cucumber.api.java.Before
import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import junit.framework.Assert
import org.junit.Rule

class BddSteps {
//        @get:Rule
//    var activityRule: ActivityScenarioRule<MainActivity>
//            = ActivityScenarioRule(MainActivity::class.java)
    private val robot = NavigateScreenRobot()
////    private val activityRule = ActivityTestRule(MainActivity::class.java, false, false)
//    @Given("^user start the application$")
//    fun user_start_app() {
//    activityRule.launchActivity(Intent())
//    activity = activityTestRule.getActivity()
////        robot.launchLoginScreen(activityRule)
//    }

    @Rule
    var activityTestRule: ActivityTestRule<MainActivity> =
        ActivityTestRule(MainActivity::class.java)

    private var activity: Activity? = null

//    @Before
//    fun setup() {
//        activityTestRule.launchActivity(Intent())
//        activity = activityTestRule.getActivity()
//    }
//
//    @After
//    fun tearDown() {
//        activityTestRule.finishActivity()
//    }
    @Given("^user start the application$")
    fun user_start_app() {
        Assert.assertNotNull(activity)
    }

    @When("^user enter valid name (\\S+)$")
    fun user_enter_name_field(name: String) {
        robot.enterNameField(name)
    }
    @And("^user close the keyboard$")
    fun user_close_the_keyboard() {
        robot.closeSoftKey()
    }
    @And("^user click (\\S+) button$")
    fun user_click_next_button(button: String) {
        robot.performClickOnButton(button)
    }
    @Then("^user expect to see Previous button on next page")
    fun i_expect_to_see_previous_button() {
        robot.isSuccessfulNavigate()
    }
    @Then ("^user verify the edit text contain (\\S+)$")
    fun user_verify_edit_text(name: String)
    {
        robot.verifyEditText(name)
    }
    @Then("^user expect to see Next button on Previous page")
    fun i_expect_to_see_next_button() {
        robot.verifyNextButton()
    }

    @And ("^user verify the content of text View")
    fun userVerifyTextView()
    {
        robot.verifyContentOfTextView()
    }
    @Then( "^user verify (\\S+) present on First Fragment")
    fun verifyElement(element:String)
    {
        robot.verifyElements(element)
    }
}