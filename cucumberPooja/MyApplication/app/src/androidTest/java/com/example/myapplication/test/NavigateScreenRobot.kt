package com.example.myapplication.test

import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.rule.ActivityTestRule
import com.example.myapplication.MainActivity
import com.example.myapplication.R
import org.hamcrest.core.AllOf.allOf

class NavigateScreenRobot {
    fun launchLoginScreen(testRule: ActivityTestRule<MainActivity>) {
        testRule.launchActivity(null)
    }
    fun enterNameField(name: String) {
        Espresso.onView(withId(R.id.editTextTextPersonName)).perform(ViewActions.clearText())
        onView(withId(R.id.editTextTextPersonName)).perform(typeText(name))
    }
    fun closeSoftKey()
    {
        Espresso.onView(withId(R.id.editTextTextPersonName)).perform(ViewActions.closeSoftKeyboard())
    }
    fun performClickOnButton(button: String)
    {
        if(button.equals("Next")) {
            Espresso.onView(withId(R.id.button_first)).perform(ViewActions.click())
        }
        if(button.equals("Previous"))
        {
            Espresso.onView(withId(R.id.button_second)).perform(ViewActions.click())
        }

    }
    fun isSuccessfulNavigate()
    {
        Espresso.onView(withId(R.id.button_second)).check(
            ViewAssertions.matches(
                ViewMatchers.withText(
                    "Previous"
                )
            )
        )
    }
    fun verifyEditText(name: String)
    {
        onView(withId(R.id.editTextTextPersonName)).check(ViewAssertions.matches(ViewMatchers.withText(name)))
    }
    fun verifyNextButton()
    {
        Espresso.onView(withId(R.id.button_first)).check(
            ViewAssertions.matches(
                ViewMatchers.withText(
                    "Next"
                )
            )
        )
    }

    fun verifyContentOfTextView()
    {
        onView(withId(R.id.textview_first)).check(ViewAssertions.matches(ViewMatchers.withText("Hello first fragment")))
    }
    fun verifyElements(element:String)
    {
        if(element.equals("TextView")) {
            onView(allOf(withId(R.id.textview_first), isDisplayed()))
        }
        else if(element.equals("Button"))
        {
            onView(allOf(withId(R.id.button_first), isDisplayed()))
        }
        else if(element.equals("EditText"))
        {
            onView(allOf(withId(R.id.editTextTextPersonName), isDisplayed()))
        }
    }
}