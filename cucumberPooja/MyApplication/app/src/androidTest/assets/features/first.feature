Feature: my application


  @abcd
  Scenario Outline: verify data enter to edit test is correct
    When user enter valid name <userName>
    And user close the keyboard
    Then user verify the edit text contain <userName>
    Examples:
      |userName|
      |Pooja   |
      |Neha    |

  @first-feature
  Scenario Outline: verify functionality of next button
    When user click <buttonName> button
    Then user expect to see Previous button on next page
    Examples:
      |buttonName|
      |Next   |

  @first-feature
  Scenario: verify functionality of previous button
    When user click Next button
    And user click Previous button
    Then user expect to see Next button on Previous page

  @first-feature
  Scenario: verify text view on First Fragment
    Then user verify TextView present on First Fragment
    And user verify the content of text View

  @first-feature
  Scenario: verify all the elements on First Fragment
    Then user verify TextView present on First Fragment
    And user verify Button present on First Fragment
    And user verify EditText present on First Fragment

