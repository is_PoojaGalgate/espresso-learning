package com.example.android.testing.espresso.web.BasicSample;

import android.content.Intent;

import androidx.test.espresso.web.webdriver.DriverAtoms;
import androidx.test.espresso.web.webdriver.Locator;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.web.assertion.WebViewAssertions.webMatches;
import static androidx.test.espresso.web.sugar.Web.onWebView;
import static androidx.test.espresso.web.webdriver.DriverAtoms.findElement;
import static androidx.test.espresso.web.webdriver.DriverAtoms.getText;
import static androidx.test.espresso.web.webdriver.DriverAtoms.webClick;
import static org.hamcrest.Matchers.containsString;

@RunWith(AndroidJUnit4.class)
public class WebViewSampleTestsPooja {
    @Rule
    public ActivityTestRule<WebViewActivity> mActivityRule = new ActivityTestRule<WebViewActivity>(
            WebViewActivity.class, false, false) {
        @Override
        protected void afterActivityLaunched() {
            onWebView().forceJavascriptEnabled();
        }
    };

    @Test
    public void verifyChangeTextButton()
    {
        mActivityRule.launchActivity(withWebFormIntent());
        onWebView().withElement(findElement(Locator.NAME, "text_input"))
                .perform(DriverAtoms.webKeys("pooja"))
                .withElement(findElement(Locator.ID,"changeTextBtn"))
                .perform(webClick())
                .withElement(findElement(Locator.ID, "message"))
                .check(webMatches(getText(), containsString("pooja")));
    }
    @Test
    public void verifyChangeTextAndSubmitButton()
    {
        mActivityRule.launchActivity(withWebFormIntent());
        onWebView().withElement(findElement(Locator.NAME, "text_input"))
                .perform(DriverAtoms.webKeys("pooja"))
                .withElement(findElement(Locator.ID,"submitBtn"))
                .perform(webClick())
                .withElement(findElement(Locator.ID, "response"))
                .check(webMatches(getText(), containsString("pooja")));
    }
    private static Intent withWebFormIntent() {
        Intent basicFormIntent = new Intent();
        basicFormIntent.putExtra(WebViewActivity.KEY_URL_TO_LOAD, WebViewActivity.WEB_FORM_URL);
        return basicFormIntent;
    }

}
