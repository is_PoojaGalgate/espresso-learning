package com.example.android.testing.espresso.CustomMatcherSample;

import androidx.test.ext.junit.rules.ActivityScenarioRule;

import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;

import java.util.regex.Matcher;

import static androidx.test.core.app.ApplicationProvider.getApplicationContext;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.doesNotExist;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

public class customMatcherPooja {
    @Rule
    public ActivityScenarioRule<MainActivity> activityScenarioRule=new ActivityScenarioRule<MainActivity>(MainActivity.class);
    @Test
    public void hint_isDisplayedInEditText() {
        String hintText = getApplicationContext().getResources().getString(R.string.hint);

        onView(withId(R.id.editText)).check(matches(HintMatcher.withHint(Matchers.containsString("your coffee?"))));
//        onView(withId(R.id.editText)).check(matches(HintMatcher.withHint(hintText)));
    }
}
