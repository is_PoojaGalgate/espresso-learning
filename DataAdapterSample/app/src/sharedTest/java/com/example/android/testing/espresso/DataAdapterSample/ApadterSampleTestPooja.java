package com.example.android.testing.espresso.DataAdapterSample;

import androidx.test.espresso.Espresso;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.swipeDown;
import static androidx.test.espresso.action.ViewActions.swipeUp;
import static androidx.test.espresso.assertion.PositionAssertions.isCompletelyAbove;
import static androidx.test.espresso.assertion.PositionAssertions.isCompletelyBelow;
import static androidx.test.espresso.assertion.PositionAssertions.isCompletelyLeftOf;
import static androidx.test.espresso.assertion.PositionAssertions.isCompletelyRightOf;
import static androidx.test.espresso.assertion.PositionAssertions.isLeftAlignedWith;
import static androidx.test.espresso.assertion.PositionAssertions.isPartiallyLeftOf;
import static androidx.test.espresso.assertion.ViewAssertions.doesNotExist;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static com.example.android.testing.espresso.DataAdapterSample.LongListMatchers.withItemSize;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.is;

@RunWith(AndroidJUnit4.class)
public class ApadterSampleTestPooja {
    @Rule
    public ActivityScenarioRule<LongListActivity> activityScenarioRule=new ActivityScenarioRule<LongListActivity>(LongListActivity.class);
    @Test
    public void lastItemNotDisplayUntilScroll()
    {
       onView(withText("item: 55")).check(doesNotExist());
    }
    @Test
    public void clickOnItem55()
    {

        onData(hasEntry(equalTo(LongListActivity.ROW_TEXT), is("item: 55"))).perform(click());
        onView(withId(R.id.selection_row_value)).check(matches(withText("55")));
    }
    @Test
    public void clickItemAtPosition()
    {
        onData(allOf()).inAdapterView(withId(R.id.list)).atPosition(30).perform(click());
    }
    @Test
    public void clickOnToggleButtonItem55()
    {
        onData(hasEntry(equalTo(LongListActivity.ROW_TEXT), is("item: 55"))).onChildView(withId(R.id.rowToggleButton)).perform(click());
    }
    @Test
    public void clickOnRowContex()
    {
        onData(hasEntry(equalTo(LongListActivity.ROW_TEXT), is("item: 56"))).onChildView(withId(R.id.rowContentTextView)).perform(click());
        onView(withId(R.id.selection_row_value)).check(matches(withText("56")));
    }
    @Test
    public void verifyLeftView()
    {
        onView(withId(R.id.selection_row)) .check(isCompletelyLeftOf(withId(R.id.selection_row_value)));
//        onView(withId(R.id.selection_row)) .check(isPartiallyLeftOf(withId(R.id.selection_row_value)));
    }
    @Test
    public void verifyRightView()
    {
        onView(withId(R.id.selection_row_value)) .check(isCompletelyRightOf(withId(R.id.selection_row)));
    }
    @Test
    public void verifyAboveView()
    {
        onView(withId(R.id.selection_row_value)) .check(isCompletelyAbove(withId(R.id.list)));
        onView(withId(R.id.selection_row)) .check(isCompletelyAbove(withId(R.id.list)));
    }
    @Test
    public void verifyBelowView()
    {
        onView(withId(R.id.list)) .check(isCompletelyBelow(withId(R.id.selection_row)));
    }

    @Test
    public void swipeUpDownTheScreen()
    {
        onView(withId(R.id.list)).perform(swipeUp());
        onView(withId(R.id.list)).perform(swipeDown());
    }


}
