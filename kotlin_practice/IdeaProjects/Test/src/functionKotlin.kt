 fun main(args: Array<String>) {
        call()
        callargument("Pooja")
        println("pooja is return in ${callargumentAndReturn("Pooja")} min")
        setPoojaMood("sad")
        setPoojaMood()
    }

    fun call() {
        println("hi i am calling Pooja")
    }

    fun callargument(name: String) {
        println("hi i am calling $name")
    }

    fun callargumentAndReturn(name: String): Int {
        val time = 10
        println("hi i am calling $name")
        return time
    }

    fun setPoojaMood(mood: String = "happy") {
        println("Pooja's mood is $mood")
    }
