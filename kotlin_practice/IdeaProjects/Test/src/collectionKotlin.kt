fun main() {
    //immutable list
    val friends= listOf("aishu","kshitu","dharma","harshya")
    println(friends.sorted())
    println(friends.last())
    println(friends.contains("pooja"))
    println(friends.size)

    //mutable
    val family= arrayListOf("aai","pappa","bro","best friend")
    family.add(0,"hubby")
    println(family)
    println(family.indexOf("aai"))
    println(family.random())
    family.remove("best friend")
    println(family)

    //map -key value pairs .unmutable,unorder
    val phoneno= mapOf("pooja" to 9763,"tushar" to 8788)
    println(phoneno["pooja"])
    println(phoneno.get("tushar"))
    println(phoneno.getOrDefault("mini",2345))
    println(phoneno.values)

    //mutable version of map
    val surname= hashMapOf("dipali" to "kardile","harshada" to "ghanvat", "partiksha" to "engale")
    surname ["dipak"]="thosar"
    println(surname)
    surname.put("pooja","galgate")
    println(surname)
    surname.remove("pooja")
    println(surname)




}