fun main(args: Array<String>) {
    println(args.contentToString())
    //Escaped string
    println("he said that \"you are beautyful\"")
    println("hi \ni am pooja")
    println("Trying to use back\b slash")
    println("Remove everything\r before ")
    println(" give me \$ 8")

    //Raw String
    val para=""" |Long ago
        |there is a queen,
        |her name is Pooja""".trimMargin()
    println(para)
    val paragraph=""" .Long ago
        .there is a queen,
        .her name is Pooja""".trimMargin( ".")// it change by default | to .
    println(paragraph)
    for(char in para)
    {
        println(char)
    }

    val str1: String="Pooja Galgate"
    val value=str1.contentEquals("Pooja Galgate")
    println(value)

    val value2=str1.contains("pooja",true)
    println(value2)

    println(str1.toUpperCase())
    println(str1.toLowerCase())

    val num=6
    println(num.toString())

    //String Template
    val subsequence=str1.subSequence(2,8)
    println(subsequence)

    println("$str1 got $num percentage")

    println("$str1 has ${str1.length} characters")
}