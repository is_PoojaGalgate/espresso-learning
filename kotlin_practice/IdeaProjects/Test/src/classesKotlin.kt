class classesKotlin(name:String) {
    init {
        println("i am in primary constructor init function")
    }
    constructor(name:String, i:Int) : this(name ) {
        println("i am in secondary constructor $name, $i")

    }
}

fun main(args: Array<String>) {
    val classesKotlinObj=classesKotlin("pooja",1)
}