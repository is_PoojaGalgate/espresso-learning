package com.example.myapplication
// 'app-debug.apk'
import android.util.Log
import android.view.KeyEvent
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*

import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.example.myapplication.R.id
import org.hamcrest.Matchers
import org.hamcrest.Matchers.*
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class BasicTests {

    @get:Rule
    var activityRule: ActivityScenarioRule<MainActivity>
            = ActivityScenarioRule(MainActivity::class.java)


    @Test
    fun clearEditText()
    {
        onView(withId(id.editTextTextPersonName)).perform(clearText()).check(matches(withText("")))
    }
    @Test
    fun NavigatePage() {
        onView(withId(id.button_first)).check(matches(isDisplayed())).perform(click())
        onView(withId(id.button_second)).check(matches(withText("Previous")))
        onView(withId(id.button_second)).perform(pressBackUnconditionally())
    }
    @Test
    fun verifyHelloFirstFragmentTextView()
    {
        Espresso.onView(withId(id.textview_first)).check(matches(withText("Hello first fragment")))
    }
    @Test
    fun enterTextInEditText()
    {
        val firstName = "Pooja"
        Espresso.onView(withId(id.editTextTextPersonName)).perform(clearText())
        Espresso.onView(withId(id.editTextTextPersonName)).perform(typeText(firstName)).check(matches(withText(firstName)));
    }
    @Test
    fun replaceTextInEditText()
    {
        val firstName = "Pooja"
        Espresso.onView(withId(id.editTextTextPersonName)).perform(replaceText(firstName)).check(matches(withText(firstName)))
    }

    @Test
    fun closeSoftKey()
    {
        val firstName = "Pooja"
        Espresso.onView(withId(id.editTextTextPersonName)).perform(click())
        Espresso.onView(withId(id.editTextTextPersonName)).perform( closeSoftKeyboard())
    }
    @Test
    fun performDoubleClick()
    {

        Espresso.onView(withId(id.button_first)).perform(doubleClick())
    }
    @Test
    fun performLongClick()
    {
        Espresso.onView(withId(id.button_first)).perform(longClick())
    }
    @Test
    fun pressBackButtonforExitApplication()
    {
        Espresso.onView(withId(id.button_first)).perform(pressBackUnconditionally())
    }
    @Test
    fun findUniqueComponent()
    {
        //logical based matcher
        onView(allOf(withId(id.button_first), withText(R.string.next))).check(matches(isDisplayed()))
        onView(Matchers.anyOf(withId(id.button_first), withText("abcv"))).check(matches(isDisplayed()))
        Log.e("result", "pass")

        //Text based matchers
        onView(withText(equalToIgnoringCase("next"))).check(matches(isDisplayed()))
        onView(withText(equalToIgnoringWhiteSpace("Next "))).check(matches(isDisplayed())) //white spaces allows before or after the text .not between the text.
        onView(withText(containsString("ex"))).check(matches(isDisplayed()))
        onView(withText(startsWith("Ne"))).check(matches(isDisplayed()))
        onView(withText(endsWith("xt"))).check(matches(isDisplayed()))

        onView(allOf(withId(id.button_first), isCompletelyDisplayed()))
        onView(allOf(withId(id.button_first), isEnabled()))
        onView(allOf(withId(id.button_first), isClickable()))

    }
}



//run commands
//am instrument [flags] <test_package>/<runner_class>


//navigate to this path is imp to start adb  = C:\Users\pooja.galgate\AppData\Local\Android\Sdk\platform-tools
//adb shell am instrument -w com.example.myapplication.test/androidx.test.runner.AndroidJUnitRunner (run all tests)
//adb shell am instrument -w -e class com.example.myapplication.BasicTests \com.example.myapplication.test/androidx.test.runner.AndroidJUnitRunner  (run only trial testcases)
//adb shell am instrument -w -e class com.example.myapplication.BasicTests#enterTextInEditText \com.example.myapplication.test/androidx.test.runner.AndroidJUnitRunner  (run only particular method)
//adb shell am instrument -w -e class com.example.myapplication.BasicTests,com.example.myapplication.MainActivityTest2 \com.example.myapplication.test/androidx.test.runner.AndroidJUnitRunner (run from multiple classes)