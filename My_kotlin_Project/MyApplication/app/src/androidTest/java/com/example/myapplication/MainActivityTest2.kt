package com.example.myapplication


import android.view.View
import android.view.ViewGroup
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class MainActivityTest2 {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun mainActivityTest2() {
        val appCompatEditText = onView(
                allOf(withId(R.id.editTextTextPersonName), withText("Name"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.nav_host_fragment),
                                        0),
                                2),
                        isDisplayed()))
        appCompatEditText.perform(replaceText("Hi"))

        val appCompatEditText2 = onView(
                allOf(withId(R.id.editTextTextPersonName), withText("Hi"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.nav_host_fragment),
                                        0),
                                2),
                        isDisplayed()))
        appCompatEditText2.perform(closeSoftKeyboard())

        val materialButton = onView(
                allOf(withId(R.id.button_first), withText("Next"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.nav_host_fragment),
                                        0),
                                1),
                        isDisplayed()))
        materialButton.perform(click())

        val button = onView(
                allOf(withId(R.id.button_second), withText("PREVIOUS"),
                        withParent(withParent(withId(R.id.nav_host_fragment))),
                        isDisplayed()))
        button.check(matches(isDisplayed()))

        val materialButton2 = onView(
                allOf(withId(R.id.button_second), withText("Previous"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.nav_host_fragment),
                                        0),
                                1),
                        isDisplayed()))
        materialButton2.perform(click())

        val textView = onView(
                allOf(withId(R.id.textview_first), withText("Hello first fragment"),
                        withParent(withParent(withId(R.id.nav_host_fragment))),
                        isDisplayed()))
        textView.check(matches(withText("Hello first fragment")))

        val button2 = onView(
                allOf(withId(R.id.button_first), withText("NEXT"),
                        withParent(withParent(withId(R.id.nav_host_fragment))),
                        isDisplayed()))
        button2.check(matches(isDisplayed()))
    }

    private fun childAtPosition(
            parentMatcher: Matcher<View>, position: Int): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }
}
