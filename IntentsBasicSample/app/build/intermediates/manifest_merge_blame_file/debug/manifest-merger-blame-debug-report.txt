1<?xml version="1.0" encoding="utf-8"?>
2<!--
3~ Copyright (C) 2015 The Android Open Source Project
4~
5~ Licensed under the Apache License, Version 2.0 (the "License");
6~ you may not use this file except in compliance with the License.
7~ You may obtain a copy of the License at
8~
9~   http://www.apache.org/licenses/LICENSE-2.0
10~
11~ Unless required by applicable law or agreed to in writing, software
12~ distributed under the License is distributed on an "AS IS" BASIS,
13~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
14~ See the License for the specific language governing permissions and
15~ limitations under the License.
16-->
17<manifest xmlns:android="http://schemas.android.com/apk/res/android"
18    package="com.example.android.testing.espresso.IntentsBasicSample"
19    android:versionCode="1"
20    android:versionName="1.0" >
21
22    <uses-sdk
23        android:minSdkVersion="16"
23-->D:\Espresso\bitbucket espresso\espresso-learning\IntentsBasicSample\app\src\main\AndroidManifest.xml
24        android:targetSdkVersion="30" />
24-->D:\Espresso\bitbucket espresso\espresso-learning\IntentsBasicSample\app\src\main\AndroidManifest.xml
25
26    <uses-permission android:name="android.permission.CALL_PHONE" />
26-->D:\Espresso\bitbucket espresso\espresso-learning\IntentsBasicSample\app\src\main\AndroidManifest.xml:21:5-68
26-->D:\Espresso\bitbucket espresso\espresso-learning\IntentsBasicSample\app\src\main\AndroidManifest.xml:21:22-66
27
28    <application
28-->D:\Espresso\bitbucket espresso\espresso-learning\IntentsBasicSample\app\src\main\AndroidManifest.xml:23:5-39:19
29        android:appComponentFactory="androidx.core.app.CoreComponentFactory"
29-->[androidx.core:core:1.2.0] C:\Users\pooja.galgate\.gradle\caches\transforms-2\files-2.1\c66c83cea6812b8fb9b32c6de6528c32\core-1.2.0\AndroidManifest.xml:24:18-86
30        android:debuggable="true"
31        android:icon="@drawable/ic_launcher"
31-->D:\Espresso\bitbucket espresso\espresso-learning\IntentsBasicSample\app\src\main\AndroidManifest.xml:24:13-49
32        android:label="@string/app_name"
32-->D:\Espresso\bitbucket espresso\espresso-learning\IntentsBasicSample\app\src\main\AndroidManifest.xml:25:13-45
33        android:testOnly="true"
34        android:theme="@style/AppTheme" >
34-->D:\Espresso\bitbucket espresso\espresso-learning\IntentsBasicSample\app\src\main\AndroidManifest.xml:26:13-44
35        <activity
35-->D:\Espresso\bitbucket espresso\espresso-learning\IntentsBasicSample\app\src\main\AndroidManifest.xml:27:9-34:20
36            android:name="com.example.android.testing.espresso.IntentsBasicSample.DialerActivity"
36-->D:\Espresso\bitbucket espresso\espresso-learning\IntentsBasicSample\app\src\main\AndroidManifest.xml:28:17-47
37            android:label="@string/app_name" >
37-->D:\Espresso\bitbucket espresso\espresso-learning\IntentsBasicSample\app\src\main\AndroidManifest.xml:29:17-49
38            <intent-filter>
38-->D:\Espresso\bitbucket espresso\espresso-learning\IntentsBasicSample\app\src\main\AndroidManifest.xml:30:13-33:29
39                <action android:name="android.intent.action.MAIN" />
39-->D:\Espresso\bitbucket espresso\espresso-learning\IntentsBasicSample\app\src\main\AndroidManifest.xml:31:17-68
39-->D:\Espresso\bitbucket espresso\espresso-learning\IntentsBasicSample\app\src\main\AndroidManifest.xml:31:25-66
40
41                <category android:name="android.intent.category.LAUNCHER" />
41-->D:\Espresso\bitbucket espresso\espresso-learning\IntentsBasicSample\app\src\main\AndroidManifest.xml:32:17-76
41-->D:\Espresso\bitbucket espresso\espresso-learning\IntentsBasicSample\app\src\main\AndroidManifest.xml:32:27-74
42            </intent-filter>
43        </activity>
44        <activity
44-->D:\Espresso\bitbucket espresso\espresso-learning\IntentsBasicSample\app\src\main\AndroidManifest.xml:35:9-37:51
45            android:name="com.example.android.testing.espresso.IntentsBasicSample.ContactsActivity"
45-->D:\Espresso\bitbucket espresso\espresso-learning\IntentsBasicSample\app\src\main\AndroidManifest.xml:36:17-49
46            android:label="@string/app_name" />
46-->D:\Espresso\bitbucket espresso\espresso-learning\IntentsBasicSample\app\src\main\AndroidManifest.xml:37:17-49
47    </application>
48
49</manifest>
