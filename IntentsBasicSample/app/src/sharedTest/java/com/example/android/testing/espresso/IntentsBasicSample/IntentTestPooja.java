package com.example.android.testing.espresso.IntentsBasicSample;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;

import androidx.test.espresso.action.ViewActions;
import androidx.test.espresso.intent.rule.IntentsTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.intent.Intents.intended;
import static androidx.test.espresso.intent.Intents.intending;
import static androidx.test.espresso.intent.matcher.ComponentNameMatchers.hasShortClassName;
import static androidx.test.espresso.intent.matcher.IntentMatchers.hasAction;
import static androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static androidx.test.espresso.intent.matcher.IntentMatchers.hasData;
import static androidx.test.espresso.intent.matcher.IntentMatchers.isInternal;
import static androidx.test.espresso.intent.matcher.IntentMatchers.toPackage;
import static androidx.test.espresso.matcher.ViewMatchers.isRoot;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.not;

@RunWith(AndroidJUnit4.class)
public class IntentTestPooja {
    private static final String VALID_PHONE_NUMBER = "9763415075";

    private static final Uri INTENT_DATA_PHONE_NUMBER = Uri.parse("tel:" + VALID_PHONE_NUMBER);


    @Rule
    public IntentsTestRule<DialerActivity>intentsTestRule= new IntentsTestRule<>(DialerActivity.class);
//    public ActivityTestRule<DialerActivity>activityTestRule=new ActivityTestRule<>(DialerActivity.class);


    @Test
    public void intendedCheckSampleTest()
    {
        onView(withId(R.id.edit_text_caller_number))
                .perform(typeText(VALID_PHONE_NUMBER), closeSoftKeyboard());
        onView(withId(R.id.button_call_number)).perform(click());

        intended(hasAction(Intent.ACTION_CALL));
        intended(hasData(INTENT_DATA_PHONE_NUMBER));

    }
//    @Test
//    public void anotherWayTOLAunchIntent()
//    {
//        onView(withId(R.id.edit_text_caller_number))
//                .perform(typeText(VALID_PHONE_NUMBER), closeSoftKeyboard());
//        onView(withId(R.id.button_call_number)).perform(click());
//        Intent i=new Intent();
//        activityTestRule.launchActivity(i);
//    }
    @Test
    public void intendingCheckSampleTest() {
//stub is ready
//        final Intent resultData = new Intent();
//        resultData.putExtra("phone", VALID_PHONE_NUMBER);
//        Instrumentation.ActivityResult result =
//                new Instrumentation.ActivityResult(Activity.RESULT_OK, resultData);

//        intending(hasComponent(hasShortClassName(".ContactsActivity"))).respondWith(result);
//        intending(toPackage("com.android.contacts")).respondWith(result);
//        intending(hasComponent(hasShortClassName(".ContactsActivity")))
//                .respondWith(new Instrumentation.ActivityResult(Activity.RESULT_OK,
//                        ContactsActivity.createResultData(VALID_PHONE_NUMBER)));

//        intending(hasAction(Intent.ACTION_CALL)).respondWith(result);
//        onView(isRoot()).perform(ViewActions.pressBack());

//        onView(withId(R.id.button_pick_contact)).perform(click());
//        onView(withId(R.id.edit_text_caller_number)).check(matches(withText(VALID_PHONE_NUMBER)));



        intending(hasComponent(hasShortClassName(".ContactsActivity")))
                .respondWith(new Instrumentation.ActivityResult(Activity.RESULT_OK,
                        ContactsActivity.createResultData(VALID_PHONE_NUMBER)));

        onView(withId(R.id.button_pick_contact)).perform(click());

        // Check that the number is displayed in the UI.
        onView(withId(R.id.edit_text_caller_number))
                .check(matches(withText(VALID_PHONE_NUMBER)));
    }

}
